<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import ="episunsa.edu.pe.*" %>
<%@ page import ="javax.jdo.PersistenceManager" %>
<%@ page import ="javax.jdo.Query" %>
<%@ page import ="java.util.List" %>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="css/estilos.css"/>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
	<link rel="stylesheet" href="icomoon/font.css">
	<script src="js/javascript.js"></script>
	<script src="js/main.js"></script>
<title>La Iberica</title>
</head>
<body>
	<input type="checkbox" id="cerrar">
	<label for="cerrar" id="btn-cerrar"><img src="img/aspa.png" width="50px" height="50px" alt=""></label>
	<div class="modal">
		<div class="contenido2">
			<div class="sesion">
			<div class="wrap" id="wrap2">
				<form method="post" action="crear" class="formulario" name="formulario_rigistro">
					<div>
						<h1> Registrate </h1>
						<div class="input-group">
							<input type="text" id="email" name="nombre" />
							<label class="label" for="nombre">Nombre : </label>
						</div>
						<div class="input-group">
							<input type="text" id="email" name="apellido" />
							<label class="label" for="nombre">Apellido : </label>
						</div>
						<div class="input-group">
							<input type="text" id="email" name="direccion" />
							<label class="label" for="nombre">Direccion : </label>
						</div>
						<div class="input-group">
							<input type="text" id="email" name="usuario" />
							<label class="label" for="nombre">Usuario : </label>
						</div>
						<div class="input-group">
							<input type="password" id="pass" name="contraseña" />
							<label class="label" for="nombre">Contraseña : </label>
						</div>
						<div class="input-group">
							<input type="password" id="pass" name="contraseña2" />
							<label class="label" for="nombre">Verifique : </label>
						</div>
						<input type="submit" id="btn-submit" value="Registrarme">
					</div>
				</form>
			</div>
			</div>
		</div>
	</div>
<div class="social">
		<ul>
			<li><a href="https://www.facebook.com/LaIbericaPeru/?fref=ts" target="_blank" class="icon-facebook"></a></li>
			<li><a href="https://twitter.com/?lang=es" target="_blank" class="icon-twitter"></a></li>
			<li><a href="https://mail.google.com/mail/" target="_blank" class="icon-google-plus"></a></li>
			<li><a href="https://www.youtube.com/watch?v=TzwJI0WYLVk" target="_blank" class="icon-youtube"></a></li>
			<li><a href="https://accounts.google.com/ServiceLogin?service=blogger&hl=es&passive=1209600&continue=https://www.blogger.com/home" target="_blank" class="icon-blogger"></a></li>
		</ul>
	</div>
	<div class="logo">
		<h1>LA IBERICA</h1>
	</div>
	<div class="menu">
		<ul>
			<li><a class="letra"  href="empresa.html">Empresa</a></li>
			<li><a class="letra" href="registro.jsp">Registro</a></li>
			<li><a class="letra"  href="productos.jsp">Productos</a></li>
		</ul>
	</div>
	<div class="articulos">
					<div class="imagen-zoom">
						<img id="zoom" src="img/choco2.jpg" alt="">
					</div>
					<div class="imagen-zoom">
						<img id="zoom" src="img/chocolate.jpg" alt="">
					</div>
					<div class="imagen-zoom-2">
						<img id="zoom" src="img/colo1.jpeg" alt="">
					</div>
					<div class="imagen-zoom-2">
						<img id="zoom" src="img/choco3.jpg" alt="">
					</div>
					<div class="imagen-zoom-2">
						<img id="zoom" src="img/images.jpg" alt="">
					</div>
	</div>
	<div class="informacion">
		<div class="circulos"><a href="deliveri.jsp"> <img src="img/home.png" width="80px" height="80px" alt=""></a><h3>DELIVERI</h3></div>
		<div class="circulos"><a href="ingresar.html" ><img src="img/home.png" width="80px" height="80px" alt=""></a><h3>COMPRAR</h3></div>
		<div class="circulos"><a href="pedidos.jsp" ><img src="img/home.png" width="80px" height="80px" alt=""></a><h3>PEDIDOS</h3></div>
	</div>
	<div class="comentarios">
		<h1>Comentarios</h1>
		<center>
		<%PersistenceManager pm = PMFComentarios.get().getPersistenceManager();
		Query q = pm.newQuery(Comentario.class);
		List<Comentario> pro = (List<Comentario>) q.execute(); 
		
		for(int i = 0; i< pro.size(); i++){
			out.println("<label class='comen'>"+pro.get(i)+"</label><br/><br/>");
		}
			%>
			</center>
		<form action="publicar" method="get">
		<input type="text" id="coment" size="122" name="comentario" placeholder="Deje algun comentario" />
		<input type="text" name="nombre" size="6" placeholder="Usuario" />
		<input type="submit" value="Publicar" /> 
		</form>
	</div>
	<div class="footer">
		<div class="pie-de-pagina">
				<p>Copyright © 2016 Sistemas</p>
				<div class="prf">
					<a href=""><p>¿Quienes somos?</p></a>
					<a href=""><p>Política De Privacidad</p></a>
					<a href=""><p>Términos de Servicio</p></a>
				</div>
				<div class="img">
					<a href="https://www.facebook.com/LaIbericaPeru/?fref=ts"><img id="img111" src="img/facebook-png.png" width="32px" alt=""></a>
					<a href="https://mail.google.com/mail/"><img id="img112" src="img/google+-png.png" width="22px" alt=""></a>
					<a href="https://mail.google.com/mail/"><img id="img113" src="img/google+-png.png" width="20px" height="20px" alt=""></a>
				</div>
			</div>
	</div>
</body>
</html>