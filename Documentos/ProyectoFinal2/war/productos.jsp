<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import ="episunsa.edu.pe.*" %>
<%@ page import ="javax.jdo.PersistenceManager" %>
<%@ page import ="javax.jdo.Query" %>
<%@ page import ="java.util.List" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="icomoon/font.css">
	<title>La Iberica</title>
	<link rel="stylesheet" href="css/estilos.css">
	<script src="js/javascript.js"></script>
	<script src="js/main.js"></script>	
</head>
<body>
	
	<div class="social">
		<ul>
			<li><a href="https://www.facebook.com/LaIbericaPeru/?fref=ts" target="_blank" class="icon-facebook"></a></li>
			<li><a href="https://twitter.com/?lang=es" target="_blank" class="icon-twitter"></a></li>
			<li><a href="https://mail.google.com/mail/" target="_blank" class="icon-google-plus"></a></li>
			<li><a href="https://www.youtube.com/watch?v=TzwJI0WYLVk" target="_blank" class="icon-youtube"></a></li>
			<li><a href="https://accounts.google.com/ServiceLogin?service=blogger&hl=es&passive=1209600&continue=https://www.blogger.com/home" target="_blank" class="icon-blogger"></a></li>
		</ul>
	</div>
	<div class="logo">
		<h1>LA IBERICA</h1>
	</div>
	<div class="menu">
	<!-- por el momento hice que todo fuera html ... en un futoro sera jsp(algunos :v) -->
		<ul>
			<li><a class="letra"  href="empresa.html">Empresa</a></li>
			<li><a class="letra" href="registro.jsp">Registrarme</a></li>
			<li><a class="letra"  href="productos.jsp">Productos</a></li>
			<li><a class="letra" href="registrarProducto.jsp">Nuevo Producto</a></li>
		</ul>
	</div>
	<div class="articulos">
		<center>
		<table border=3 >
		<% PersistenceManager asd = PMFproducto.get().getPersistenceManager();
		Query queri = asd.newQuery(Producto.class);
		List<Producto> los = (List<Producto>) queri.execute();
		
		out.println("<tr><th>Codigo</th>");
		out.println("<th>Nombre</th>");
		out.println("<th>Costo</th>");
		out.println("<th>X Mayor</th></tr>");
		
		for(int i = 0; i < los.size(); i++){
			out.println("<tr><td>" + los.get(i).getId() + "</td><td>" + los.get(i).getNombre() + "</td>");
			out.println("<td>" + los.get(i).getCosto() + "</td><td>" + los.get(i).getCosto2() + "</td></tr>");
		}
		%>
		</table>
		</center>
	</div>
	<div class="informacion">
		<div class="circulos"><a href="deliveri.jsp"> <img src="img/home.png" width="80px" height="80px" alt=""></a><h3>DELIVERI</h3></div>
		<div class="circulos"><a href="ingresar.html" ><img src="img/home.png" width="80px" height="80px" alt=""></a><h3>COMPRAR</h3></div>
		<div class="circulos"><a href="pedidos.jsp" ><img src="img/home.png" width="80px" height="80px" alt=""></a><h3>PEDIDOS</h3></div>
	</div>
	<div class="comentarios">
		<h1>Comentarios</h1>
		<center>
		
		<% PersistenceManager pm = PMFComentarios.get().getPersistenceManager();
		Query q = pm.newQuery(Comentario.class);
		List<Comentario> pro = (List<Comentario>) q.execute(); 
		
		for(int i = 0; i< pro.size(); i++){
			out.println("<label class='comen'>"+pro.get(i)+"</label><br/><br/>");
		}
			%>
			</center>
		<form action="publicar" method="get">
		<input type="text" id="coment" size="122" name="comentario" placeholder="Deje algun comentario" />
		<input type="text" name="nombre" size="6" placeholder="Usuario" />
		<input type="submit" value="Publicar" /> 
		</form>
	</div>
	<div class="footer">
		<div class="pie-de-pagina">
				<p>Copyright © 2016 Sistemas</p>
				<div class="prf">
					<a href=""><p>¿Quienes somos?</p></a>
					<a href=""><p>Política De Privacidad</p></a>
					<a href=""><p>Términos de Servicio</p></a>
				</div>
				<div class="img">
					<a href="https://www.facebook.com/LaIbericaPeru/?fref=ts"><img id="img111" src="img/facebook-png.png" width="32px" alt=""></a>
					<a href="https://mail.google.com/mail/"><img id="img112" src="img/google+-png.png" width="22px" alt=""></a>
					<a href="https://mail.google.com/mail/"><img id="img113" src="img/google+-png.png" width="20px" height="20px" alt=""></a>
				</div>
			</div>
	</div>
</body>
</html>