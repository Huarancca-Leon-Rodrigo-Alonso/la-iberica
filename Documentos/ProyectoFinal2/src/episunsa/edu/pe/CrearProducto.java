package episunsa.edu.pe;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class CrearProducto extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		String id = req.getParameter("idProducto");
		String nombre = req.getParameter("nombreProducto");
		String costo = req.getParameter("costo");
		String costo2 = req.getParameter("costoEspecial");
		
		RequestDispatcher rd = null;
		
		Producto a = new Producto(id, nombre, costo, costo2);
			
		PersistenceManager ppk = PMFproducto.get().getPersistenceManager();
		
		
		try{
			ppk.makePersistent(a);
			System.out.println("Producto inscrito correctamente");
			rd = req.getRequestDispatcher("/productos.jsp");
			

		}catch(Exception e){
			rd = req.getRequestDispatcher("/registrarProducto.jsp");
			System.out.println(e);
			resp.getWriter().println("Ocurrio un error, <a href='index.jsp'>vuelva a intentarlo</a>");
		}finally{
			ppk.close();
		}
		
		rd.forward(req, resp);
	}
}
