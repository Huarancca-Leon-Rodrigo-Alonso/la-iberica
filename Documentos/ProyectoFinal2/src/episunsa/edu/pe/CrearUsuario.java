package episunsa.edu.pe;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class CrearUsuario extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		String nombre = req.getParameter("nombre");
		String apellido = req.getParameter("apellido");
		String direccion = req.getParameter("direccion");
		String usuario = req.getParameter("usuario");
		String contraseña = req.getParameter("contraseña");
		String contraseña2 = req.getParameter("contraseña2");
		
		RequestDispatcher rd = null;	
		
		if(contraseña.equals(contraseña2)){
		
			Usuario u = new Usuario(nombre, apellido, direccion, usuario, contraseña);
			
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		
		try{
			pm.makePersistent(u);
			System.out.println(u.getUsuario() + " " + u.getContraseña());
			rd = req.getRequestDispatcher("/salir.html");
			

		}catch(Exception e){
			System.out.println(e);
			resp.getWriter().println("Ocurrio un error, <a href='index.jsp'>vuelva a intentarlo</a>");
		}finally{
			pm.close();
		}}
		else{
			rd = req.getRequestDispatcher("/registro.jsp");
		}
		rd.forward(req, resp);
	}
}