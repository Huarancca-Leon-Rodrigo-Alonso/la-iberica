package episunsa.edu.pe;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
@PersistenceCapable
public class Comentario {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	
	@Persistent
	private String coment;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String apellido;
	public Comentario(String Coment, String nombre, String apellido){
		this.coment = Coment;
		this.nombre = nombre;
		this.apellido = apellido;
	}
	public String getComent(){
		return this.coment;
	}
	public void setComent(String coment){
		this.coment = coment;
	}
	public Key getKey() {
		return key;
	}
	
	@Override
	public String toString() {
		String resp = nombre + " " + apellido + "<br/>" + coment;  
		return resp;
	}
}
