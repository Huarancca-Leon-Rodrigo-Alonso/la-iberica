package episunsa.edu.pe;

import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;

@SuppressWarnings("serial")
public class logIn extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setContentType("text/html");
		Lista car = new Lista();
		
		Authenticator a = new Authenticator();
		String user = req.getParameter("usuario");
		String contr = req.getParameter("contraseņa");
		RequestDispatcher rd = null;
		
		String result = a.autenticate(user,contr);
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		
		int costo = 0;
		
		if(result.equals("accede")){
			
			if(car.getBurro().size() > 0 ){
				
				Date dato = new Date();
				out.println("  Usuario  " + user );
				out.println("  Fecha  -- " + dato.toString());
				for(int i = 0; i < car.getBurro().size() ; i++){
					out.println(car.getBurro().get(i).getNombre() + " " + car.getBurro().get(i).getCosto());
					costo = costo + Integer.parseInt(car.getBurro().get(i).getCosto());
				}
				
				out.print("  Costo total de -> " + costo);
				out.println(" Porfavor acercarse a cualquiera de nuestros distribuidores para realizar su pedido ");
			}
			else{
				rd = req.getRequestDispatcher("/pedidos.jsp");
			rd.forward(req, resp);
			}
		}
		else{
			rd = req.getRequestDispatcher("/ingresar.html");
			rd.forward(req, resp);
		}		
	}
}
