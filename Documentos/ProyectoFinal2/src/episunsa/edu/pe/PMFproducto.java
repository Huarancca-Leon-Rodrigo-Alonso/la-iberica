package episunsa.edu.pe;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public final class PMFproducto {
    private static final PersistenceManagerFactory pmfInstance =
        JDOHelper.getPersistenceManagerFactory("transactions-optional");

    private PMFproducto() {}

    public static PersistenceManagerFactory get() {
        return pmfInstance;
    }
}