package episunsa.edu.pe;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class BuscadorProductos extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String codigo = req.getParameter("buscador");
		
		PersistenceManager aup = PMFproducto.get().getPersistenceManager();
		
		Query q = aup.newQuery(Producto.class);
		
		List<Usuario> personas = (List<Usuario>) q.execute();
		
		Lista lst = new Lista();
		RequestDispatcher rd;
		
		AuthenticatorProducto ap = new AuthenticatorProducto();
		
		Producto pro = ap.autenticate(codigo);
		if(pro != null){
			
			lst.addCarrito(pro);
			rd = req.getRequestDispatcher("/pedidos.jsp");
		}
		else{
			
			rd = req.getRequestDispatcher("/errorProducto.jsp");
		}		
		
		rd.forward(req, resp);
	}
}
