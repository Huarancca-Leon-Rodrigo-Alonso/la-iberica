package episunsa.edu.pe;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
@PersistenceCapable
public class Usuario {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String apellido;
	
	@Persistent
	private String direccion;
	
	@Persistent
	private String usuario;

	@Persistent
	private String contraseña;

	public Usuario(String nombre, String apellido, String direccion, String usuario, String contraseña) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.usuario = usuario;
		this.contraseña = contraseña;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña(){
		return contraseña;
	}
	public void setContraseña(String contraseña){
		this.contraseña= contraseña;
	}

	public Key getKey() {
		return key;
	}
	
	@Override
	public String toString() {
		String resp = nombre + " : " + apellido + " : " + direccion + " : " + usuario;  
		return resp;
	}
}
