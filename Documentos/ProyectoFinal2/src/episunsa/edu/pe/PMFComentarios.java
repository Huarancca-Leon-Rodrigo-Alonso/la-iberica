package episunsa.edu.pe;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

public final class PMFComentarios {
    private static final PersistenceManagerFactory pmfInstance =
        JDOHelper.getPersistenceManagerFactory("transactions-optional");

    private PMFComentarios() {}

    public static PersistenceManagerFactory get() {
        return pmfInstance;
    }
}