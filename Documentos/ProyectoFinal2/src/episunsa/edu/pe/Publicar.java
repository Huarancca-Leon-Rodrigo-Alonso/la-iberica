package episunsa.edu.pe;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Publicar extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String coment = req.getParameter("comentario");
		String nombre = req.getParameter("nombre : ");
		String apellido = "";
		
		RequestDispatcher rd = null;
		PersistenceManager pc = PMFComentarios.get().getPersistenceManager();
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		
		String current = "";
		PersistenceManager user = PMF.get().getPersistenceManager();
		Query q = user.newQuery(Usuario.class);
		

		try{
			List<Usuario> personas = (List<Usuario>) q.execute();
		
			
			for(int i = 0; i < personas.size(); i++){
				
				if(personas.get(i).getUsuario().equals(nombre)){
					current = nombre;
					apellido = personas.get(i).getApellido();
				}
			}
		
		}
		catch(Exception e){
			System.out.println(e.getMessage()+"  algun error en leer datos");
		}	
		finally{
			 q.closeAll();
			 }	
		
		if(!coment.equals("")&& !current.equals("")){
			try{
				Comentario asd = new Comentario(coment, current, apellido);
				pc.makePersistent(asd);
			}
			catch(Exception e){
				System.out.println("this coment no can write in the page u.u");
			}
			finally{
				rd = req.getRequestDispatcher("/index.jsp");
			}
		}
		else{
			rd = req.getRequestDispatcher("/index.jsp");
		}		
		
		rd.forward(req, resp);
	}
}