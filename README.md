This is my modification, and this is a last  version of 'la iberica'.

Haciendo el mejor chocolate desde 1909

Esta historia se inicia a principios del siglo XX en el sur del Perú, en Arequipa, una ciudad rodeada de volcanes, reconocida por su cultura artística tallada en la piedra volcánica "sillar", que dio lugar a la arquitectura mestiza colonial más original de América, por lo que hoy es considerada "Patrimonio de la Humanidad".

Juan Vidaurrázaga Menchaca, joven empresario español, es recibido por Arequipa y en 1909, asociando la calidad del cacao peruano con las deliciosas recetas europeas, decide fundar la Fábrica de Chocolates "La Ibérica", deleitando desde sus inicios el gusto de los exigentes consumidores locales.

El chocolate para taza fue el primer producto desarrollado y su consumo se hizo costumbre arraigada en los arequipeños.

A los pocos años amplía su variedad de productos elaborando chocolates de leche y fondant para barras y tabletas, y posteriormente finos bombones de chocolate con exquisitos rellenos.

Luego instala una línea para fabricar mazapanes, deliciosas masitas hechas a base de almendras del Perú, conocidas también como nueces del Brasil o castañas.

Después desarrolló las líneas de toffee (caramelos blandos de leche con diferentes sabores naturales) y de turrón (suave "nougat" de almendras del Perú tostadas y miel de abeja). Posteriormente se procedió a la fabricación de figuras de chocolate.

La centenaria empresa tiene hoy una moderna fábrica ubicada en el Parque Industrial de Arequipa y cuenta con tiendas exclusivas localizadas en zonas estratégicas de Arequipa, Lima y principales ciudades del país, así como una amplia red comercial de tiendas minoristas que le permiten cubrir la atención de su creciente mercado
